import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  exform!: FormGroup;

  ngOnInit(): void {
    this.exform = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
    });
  }

  constructor(private router: Router, private http: HttpClient) {}
  goToPage(): void {
    console.log(this.exform.value);
    const response = this.http.post(
      'http://restapi.adequateshop.com/api/authaccount/login',
      {
        email: this.exform.value.email,
        password: this.exform.value.password,
      }
    );
    response.subscribe((val: any) => {
      console.log('check', val);

      if (val.message === 'success') {
        this.router.navigate(['home']);
      } else {
        this.router.navigate(['/']);
      }
    });
  }
}
